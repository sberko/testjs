import requests
import json

cnt = {
    '+': lambda a, b: a - b,
    '-': lambda a, b: a + b + 8,
    '*': lambda a, b: a % b if b != 0 else 42,
    '/': lambda a, b: a // b if b != 0 else 42,
}


def oper(a, iter_start=0):
    for c in range(iter_start, len(a)):
        if a[c].lstrip('-+').isdigit() and a[c + 1].lstrip('-+').isdigit() and a[c + 2] in '/*+-':
            a[c:c + 3] = [str(cnt[a[c + 2]](int(a[c]), int(a[c + 1])))]
            if len(a) > 1:
                return oper(a, c-1)
            else:
                return int(a[0])
        else:
            return oper(a, c + 1)


end_point = 'https://www.eliftech.com/school-task'
resp = requests.get(end_point)
if resp.ok:
    jresp = json.loads(resp.text)
    print("Start")
    res_list = []
    for num, expr in enumerate(jresp['expressions']):
        res_list.append(oper(expr.split(' ')))
        print('"{}" -> {}'.format(expr, res_list[num]))
    req = {'id': jresp['id'], "results": res_list}
    post_res = requests.post(end_point, json=req)
    if post_res.ok:
        print(post_res.text)
else:
    print("Some problem with server")
